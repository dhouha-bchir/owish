import DS from 'ember-data';

export default DS.Model.extend({
  date_publication:DS.attr('date'),
  formatted_date_publication:DS.attr('string'),
  title:DS.attr('string'),
  content:DS.attr('string'),
  owner:DS.attr('number'),
  firstname:DS.attr('string'),
  lastname:DS.attr('string')
});
