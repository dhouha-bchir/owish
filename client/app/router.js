import Ember from 'ember';
import config from './config/environment';
import user from './global';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootUrl: config.rootUrl

});

Router.map(function() {
  this.route('login', { path: '/' });
  this.route('dashboard', { path: 'admin' });
  this.route('user', { path: 'wishlist' });
  this.route('add');
});

export default Router;
