import Ember from 'ember';
import user from '../global';

export default Ember.Route.extend({
  model() {
    return this;
  },
  actions: {
    login() {
      let username= this.username;
      let password= this.password;
      let self=this;
      $.ajax('http://localhost:3000/api/login', {
        "type": 'GET',
        "dataType": 'JSON',
        data: {
          username: username,
          password: password
        },
        "success": function (data, textStatus, jqXHR) {
          if(data.length !== 0){
            if(data[0].role=="admin"){
              self.transitionTo('dashboard');
            }else{
              self.transitionTo('user');
            }
            user.setUserId(data[0].id);
            user.setUserName(data[0].firstname+" "+data[0].lastname);
          }else{
            alert("Nom d'utilisateur ou mot de passe sont incorrectes");
          }
          return data;
        },
        "error": function (jqXHR, textStatus, errorThrown) {
          window.console.log(jqXHR);
        }
      });
    }
  }
});
