import Ember from 'ember';

export default Ember.Route.extend({
  model(params){
    console.log(params);
    return Ember.RSVP.hash({
      mywish: this.store.findAll('mywish'),
      username: params.user_name
    });
  },
  setupController: function(controller, model) {
    controller.set('mywish', model.mywish);
    controller.set('username', model.username);
  }
});
