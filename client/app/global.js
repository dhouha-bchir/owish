import Ember from 'ember';

const currentUser = Ember.Object.extend({
    connectedUserId: "",
    connectedUserName: "",
    init() {
        this.set('connectedUserId', "");
        this.set('connectedUserName', "");
    },
    setUserId(id){
        this.set('connectedUserId', id);
    },
    setUserName(name){
        this.set('connectedUserName', name);
    }
});

const user = currentUser.create({});
export default user;