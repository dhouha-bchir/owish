exports.initUser = function(mySqlConnection){
    mySqlConnection.query('SELECT * from users', function (error, results, fields) {
        if(results.length == 0){
            // Administrateur
            var admin = { username: 'admin@owish.com', pass: 'admin', firstname: 'Dhouha', lastname: 'Bchir', role: 'admin' };
            mySqlConnection.query('INSERT INTO users SET ?', admin , function(err,res){
                if(err) throw err;
            });
            // Utilisateur
            var user = { username: 'user@owish.com', pass: 'user', firstname: 'Dhouha', lastname: 'bchir', role: 'user' };
            mySqlConnection.query('INSERT INTO users SET ?', user , function(err,res){
                if(err) throw err;
            });
        }
    });
};
exports.initWish = function(mySqlConnection){
    mySqlConnection.query('SELECT * from wish', function (error, results, fields) {
        if(results.length == 0){
            // wish 1
            var wish1 = { title: 'wish 1', content: 'wish 1 détails', owner: 1 };
            var wish2 = { title: 'wish 2', content: 'wish 2 détails', owner: 1 };
            var wish3 = { title: 'wish 3', content: 'wish 3 détails', owner: 1 };
            mySqlConnection.query('INSERT INTO wish SET ?',
                wish1 ,
                function(err,res){
                if(err) throw err;
            });
            mySqlConnection.query('INSERT INTO wish SET ?',
                wish2 ,
                function(err,res){
                if(err) throw err;
            });
            mySqlConnection.query('INSERT INTO wish SET ?',
                wish3 ,
                function(err,res){
                if(err) throw err;
            });
        }
    });
};