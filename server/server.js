var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
var mysql = require('mysql');
var cors = require("cors");
var moment = require('moment');
var bodyParser = require('body-parser');

app.use( bodyParser.json() );
app.use(bodyParser.urlencoded({
    extended: true
}));

// connexion au server mysql (à modifier)
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'Dhouha',
    database: 'oWish'
});

connection.connect(function(err) {
    if (err) throw err;
    console.log('connexion établie')
});

// Initialisation de la base de données

var initializeDb = require("./init.js");
initializeDb.initUser(connection);
initializeDb.initWish(connection);

// fonction de formattage de date
function formatDate(date) {
    return moment(date).format('YYYY-MM-DD')+" à "+moment(date).format('HH:mm:ss');
}

// routes
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/api/users', function(req, res) {
    connection.query('SELECT * from users', function (error, results, fields) {
        if (error) throw error;
        res.send(results);
    });
});
app.get('/', function(req, res) {
    res.send("O'Wish server");
});

app.get('/api/login', function(req, res) {
    var username = req.param('username');
    var password = req.param('password');
    connection.query('SELECT * from users WHERE username = ? and pass = ?', [username, password] , function (error, results, fields) {
        if (error) throw error;
        res.send(results);
    });
});

app.get('/mywishes', function(req, res) {
    connection.query("SELECT w.*, u.firstname, u.lastname from wish AS w INNER JOIN users AS u ON w.owner = u.id ORDER BY w.date_publication DESC", function (error, results, fields) {
        if (error) throw error;
        var rows = results.map(function(row) {
            return Object.assign({}, row, { formatted_date_publication: formatDate(row.date_publication) });
        });
        res.send({mywish: rows});
    });
});

app.post('/api/addwish', function(req, res) {
    connection.query('INSERT INTO wish SET ?', req.body, function (error, result) {
        if (error) throw error;
        res.send("successfull");
    });
});


// lancement du serveur
app.listen(port);

